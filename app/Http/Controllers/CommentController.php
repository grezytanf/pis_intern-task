<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreComment;
use App\Comment;

class CommentController extends Controller
{
    public function index()
    {
        $comment = Comment::orderBy('created_at', 'DESC')->get();
        return Comment::all();
    }

    public function store(StoreComment $request)
    {
        $comment = new Comment;
        $comment->users_id = $request->users_id;
        $comment->article_id = $request->article_id;
        $comment->comment = $request->comment;
        $validated = $request->validated();
        $comment->save();
        return response()->json(compact('comment'), 201);
    }

    public function delete($id)
    {
        $comment = Comment::find($id);
        $comment->delete();
        return response()->json([
            'message' => 'Comment Berhasil Dihapus',
            'comment' => $comment->get(),
        ]);
    }
}