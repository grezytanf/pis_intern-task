<?php

namespace App\Http\Controllers;
use App\Http\Requests\StoreUser;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index(){
        return User::all();
    }
    
    public function store(StoreUser $request){
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $validated = $request->validated();
        $user->save($request->all());
        return response()->json(compact('user','token'),201);
    }
    
    public function update(StoreUser $request, $id) {
        
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $request->only('name', 'email');
        $passwordtemp = Hash::make($request->password);
        $user->password = $passwordtemp;
        $user->update();
        return response()->json([
            'message' => 'Data Kategori Berhasil Diubah',
            'user' => $user,
            ]);
            //return response()->json(compact('user')); 
        }    
        public function delete($id){
            $user = User::find($id);
            $user->delete();
            return response()->json([
                'message' => 'Data Kategori Berhasil Dihapus',
                'user' => $user->get(),
                ]);
            }
        }