<?php

namespace App\Http\Controllers;
use App\Http\Requests\StoreArticle;
use App\Article;

use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function index(){
        return Article::all();
    }
    
    public function store(StoreArticle $request){
        $article = new Article;
        $article->users_id = $request->users_id;
        $article->categories_id = $request->categories_id;
        $article->title = $request->title;
        $article->content = $request->content;
        $validated = $request->validated();
        $article->save();
        return response()->json(compact('article'),201);
    }

    public function update(StoreArticle $request, $id) {
        $article = Article::find($id);
        $article->users_id = $request->users_id;
        $article->categories_id = $request->categories_id;
        $article->title = $request->title;
        $article->content = $request->content;
        $article->update($request->all());
        return response()->json([
            'message' => 'Data Article Berhasil Diubah',
            'article' => $article,
            ]);
        }

public function delete($id){
    $article = Article::find($id);
    $article->delete();
    return response()->json([
        'message' => 'Data Kategori Berhasil Dihapus',
        'article' => $article->get(),
        ]);
    
}
}