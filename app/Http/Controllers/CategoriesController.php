<?php

namespace App\Http\Controllers;
use App\Http\Requests\StoreCategories;
use App\Categories;

class CategoriesController extends Controller
{
    public function index(){
        return Categories::all();
    }
    
    public function store(StoreCategories $request){
        $categories = new Categories;
        $categories->name = $request->name;
        $validated = $request->validated();
        $categories->save();
        return response()->json([
            'message' => 'Data Kategori Berhasil Ditambahkan',
            'categories' => $categories,
            ]);
        }
        
        public function update(StoreCategories $request, $id) {
            $categories = Categories::find($id);
            $categories->name = $request->name;
            $categories->update($request->all());
            return response()->json([
                'message' => 'Data Kategori Berhasil Diubah',
                'categories' => $categories,
                ]);
                //return response()->json(compact('categories'));
            }    
            public function delete($id){
                $categories = Categories::find($id);
                $categories->delete();
                return response()->json([
                    'message' => 'Data Kategori Berhasil Dihapus',
                    'categories' => $categories->get(),
                    ]);
                }
            }