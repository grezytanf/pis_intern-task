<?php

use Illuminate\Http\Request;
use App\Categories;
use App\User;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    
    'middleware' => 'api',
    'prefix' => 'auth'
    
], function ($router) {
    Route::post('register', 'UserController@store');
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});

Route::group([
    
    'middleware' => 'auth'
    
], function ($router) {
    Route::prefix('user')->group(function () {
        Route::put('update/{id}', 'UserController@update');
        Route::get('/', 'UserController@index');
        Route::delete('delete/{id}', 'UserController@delete');
    });
});

Route::group([], function ($router) {
    Route::prefix('categories')->group(function () {
        Route::get('/', 'CategoriesController@index');
        Route::post('/', 'CategoriesController@store');
        Route::put('/{id}', 'CategoriesController@update');
        Route::delete('/{id}', 'CategoriesController@delete');
    });
});

Route::group([
    
    'middleware' => 'auth'
    
], function ($router) {
    Route::prefix('article')->group(function () {
        Route::get('/', 'ArticleController@index');
        Route::post('/', 'ArticleController@store');
        Route::put('update/{id}', 'ArticleController@update');
        Route::delete('delete/{id}', 'ArticleController@delete');
    });
});

Route::group([
    
    'middleware' => 'auth'
    
], function ($router) {
    Route::prefix('comment')->group(function () {
        Route::get('/', 'CommentController@index');
        Route::post('/', 'CommentController@store');
        Route::put('update/{id}', 'CommentController@update');
        Route::delete('delete/{id}', 'CommentController@delete');
    });
});